#include "vmlinux.h"
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_core_read.h>
#include <linux/limits.h>

struct {
	__uint(type, BPF_MAP_TYPE_HASH);
	__type(key, u32);
	__type(value, u64);
	__uint(max_entries, 2048);
} vruntime_map SEC(".maps");

SEC("raw_tracepoint/sched_switch")
int bpf_sched_switch_handler(struct bpf_raw_tracepoint_args *ctx) {
	struct task_struct *prev = (struct task_struct *)ctx->args[1];
	int prev_pid = BPF_CORE_READ(prev, pid);

	u64 total_utime = BPF_CORE_READ(prev, utime);
	u64 *prev_utime;

	prev_utime = (u64*)bpf_map_lookup_elem(&vruntime_map, &prev_pid);
	bpf_map_update_elem(&vruntime_map, &prev_pid, &total_utime, BPF_ANY);
	
	if (!prev_utime) {
		return 0;
	}

	u64 time_slice = total_utime - *prev_utime;

	char fmt[] = "Timeslice for user context for pid before context switching %d: %lu";
	if (time_slice) {
		bpf_trace_printk(fmt, sizeof(fmt), prev_pid, time_slice);
	}

	return 0;
}

char lic[] SEC("license") = "GPL";

#include <sys/resource.h>
#include <bpf/bpf.h>
#include <bpf/libbpf.h>
#include <stdio.h>
#include <inttypes.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>

#include "timeslice.skel.h"

static volatile int do_print_trace_pipe = 1;

void int_handler(int unused) {
	do_print_trace_pipe = 0;
}

int libbpf_print_fn(enum libbpf_print_level level, const char *format, va_list args) {
	return vfprintf(stderr, format, args);
}

void read_trace_pipe() {
	int trace_fd;

	trace_fd = open("/sys/kernel/debug/tracing/trace_pipe", O_RDONLY, 0);
	if (trace_fd < 0)
		return;

	while (do_print_trace_pipe) {
		static char buf[4096];
		ssize_t sz;

		sz = read(trace_fd, buf, sizeof(buf) - 1);
		if (sz > 0) {
			buf[sz] = 0;
			puts(buf);
		}
	}

	close(trace_fd);
}

int main(int argc, char **argv) {
	struct timeslice_bpf *obj;
	int err = 0;
	int fd;
	struct rlimit rlim = {
		.rlim_cur = 512UL << 20,
		.rlim_max = 512UL << 20,
	};

	signal(SIGINT, int_handler);

	err = setrlimit(RLIMIT_MEMLOCK, &rlim);
	if (err) {
		fprintf(stderr, "failed to change rlimit\n");
		return 1;
	}

	libbpf_set_print(libbpf_print_fn);

	obj = timeslice_bpf__open();
	if (!obj) {
		fprintf(stderr, "failed to open and/or load bpf object\n");
		return 1;
	}

	err = timeslice_bpf__load(obj);
	if (err) {
		fprintf(stderr, "failed to load bpf object %d\n", err);
		goto cleanup;
	}

	err = timeslice_bpf__attach(obj);
	if (err) {
		fprintf(stderr, "failed to attach bpf program\n");
		goto cleanup;
	}

	printf("--- Trace pipe output - <Ctrl-C> to quit ---\n");
	read_trace_pipe();

cleanup:
	timeslice_bpf__destroy(obj);
	return err != 0;
}
